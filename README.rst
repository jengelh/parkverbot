Rotational hard disks have a controversial (mis)feature which automatically
unloads the read/write heads during perceived inactivity, usually within 10
seconds. Western Digital calls it "Intellipark", but the problem is present in
other manufacturers' disks as well, and goes back to at least the mid-2000s.

Frequent parking leads to noticable latency on wakeup, and the hard retraction
is seen as an impediment to disk life. Even with various utility programs, this
parking feature is not reliably deactivatable. (See "Comparison" section
below.)

The "parkverbot" daemon will issue small read requests periodically to a random
location on disk in an effort to reset the inactivity timer in the hardware and
so prevent the dreaded head unloading. As such, its operation is
non-destructive and interoperable across different brands of hard drives and
transports.

The current block reading algorithm works reasonably well in practice: the
worst observed head parking rate was about 3 unloads/day (0.125/hour); best
figures hover around 0.5 unloads/day (0.021/hour).
