/*
 *	parkverbot - inhibit hard disk head parking
 *	Copyright © Jan Engelhardt, 2011-2013
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of the
 *	License, or (at your option) any later version.
 */
#define _FILE_OFFSET_BITS 64
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#ifdef __FreeBSD__
#	include <sys/disk.h>
#endif
#include <sys/mount.h>
#include <libHX/init.h>
#include <libHX/list.h>
#include <libHX/misc.h>
#include <libHX/option.h>
#include <libHX/string.h>

/**
 * @anchor:	anchor for linkage with @pv_bdev_lsit
 * @path:	device path, for informational purposes
 * @size:	size of the device
 * @prev_pos:	position of previous read, used to circumvent readahead
 * @fd:		file descriptor of the opened device
 */
struct pv_bdev_entry {
	struct HXlist_head anchor;
	const char *path;
	off_t size, prev_pos;
	unsigned int sector_size;
	int fd;
};

static HXLIST_HEAD(pv_bdev_list);
static struct timespec pv_req_interval = {4, 0};
static unsigned long long pv_disk_window = 16384;
static unsigned long long pv_buffer_size = 64;

static bool pv_in_window(size_t prev_pos, size_t new_pos,
    const struct pv_bdev_entry *e)
{
	size_t left, right;

	if (e->size <= pv_disk_window)
		return false;
	left  = (prev_pos <= pv_disk_window) ? 0 : prev_pos - pv_disk_window;
	right = prev_pos + pv_disk_window;
	return left <= new_pos && new_pos < right;
}

static int pv_mainloop(void)
{
	struct pv_bdev_entry *e;
	off_t new_pos;
	ssize_t read_ret;
	char *buffer;

	buffer = malloc(pv_buffer_size);
	if (buffer == NULL) {
		fprintf(stderr, "Error allocating %llu bytes\n", pv_buffer_size);
		return EXIT_FAILURE;
	}

	while (true) {
		HXlist_for_each_entry(e, &pv_bdev_list, anchor) {
			new_pos = HX_drand(0, e->size);
			/*
			 * read(2) and write(2) require the offset to be
			 * a multiple of the sector size, otherwise they
			 * will return EINVAL on FreeBSD (see PR 91149).
			 */
			if (e->sector_size != 0)
				new_pos -= new_pos % e->sector_size;
			if (pv_in_window(e->prev_pos, new_pos, e)) {
				printf("%s: %llu (in guard window)\n", e->path,
				       static_cast(unsigned long long, new_pos));
				continue;
			}
			/*
			printf("%s: %llu (prev %llu)\n", e->path,
			       static_cast(unsigned long long, new_pos),
			       static_cast(unsigned long long, e->prev_pos));
			*/
			if (lseek(e->fd, new_pos, SEEK_SET) < 0)
				fprintf(stderr, "%s: lseek: %s\n",
				        e->path, strerror(errno));

			read_ret = read(e->fd, buffer, pv_buffer_size);
			if (read_ret < 0)
				fprintf(stderr, "%s: read: %s\n",
				        e->path, strerror(errno));
			e->prev_pos = new_pos;
		}
		nanosleep(&pv_req_interval, NULL);
	}

	free(buffer);
	return EXIT_SUCCESS;
}

static bool pv_open_device(const char *path)
{
	struct pv_bdev_entry *e;
	char buf[32];
	uint64_t size;
	unsigned int sector_size = 0;
	int fd;

	fd = open(path, O_RDONLY | O_BINARY);
	if (fd < 0) {
		fprintf(stderr, "%s: %s\n", path, strerror(errno));
		return false;
	}
#ifdef __linux__
	if (ioctl(fd, BLKGETSIZE64, &size) < 0) {
		fprintf(stderr, "%s: BLKGETSIZE64: %s\n", path, strerror(errno));
		return false;
	}
#endif
#ifdef __FreeBSD__
	if (ioctl(fd, DIOCGSECTORSIZE, &sector_size) < 0) {
		fprintf(stderr, "%s: DIOCGSECTORSIZE: %s\n", path, strerror(errno));
		return false;
	}
	if (ioctl(fd, DIOCGMEDIASIZE, &size) < 0) {
		fprintf(stderr, "%s: DIOCGMEDIASIZE: %s\n", path, strerror(errno));
		return false;
	}
#endif
	e = malloc(sizeof(*e));
	if (e == NULL) {
		fprintf(stderr, "%s: %s\n", __func__, strerror(errno));
		close(fd);
		return false;
	}
	HXlist_init(&e->anchor);
	e->path = path;
	e->size = size;
	e->sector_size = sector_size;
	e->fd   = fd;
	printf("Added %s (size %s)\n", e->path,
	       HX_unit_size(buf, sizeof(buf), e->size, 1024, 0));
	HXlist_add_tail(&pv_bdev_list, &e->anchor);
	return true;
}

static bool pv_get_options(int *argc, const char ***argv)
{
	const char **arg;
	double intv = 0;
	struct HXoption options_table[] = {
		{.sh = 'b', .type = HXTYPE_ULLONG, .ptr = &pv_buffer_size,
		 .help = "Buffer size, in KB (default: 64)", .htyp = "kbytes"},
		{.sh = 'r', .type = HXTYPE_ULLONG, .ptr = &pv_disk_window,
		 .help = "Guard window size, in KB (default: 16384)",
		 .htyp = "kbytes"},
		{.sh = 't', .type = HXTYPE_DOUBLE, .ptr = &intv,
		 .help = "Interval between requests, in s (default: 4.0)",
		 .htyp = "sec"},
		HXOPT_AUTOHELP,
		HXOPT_TABLEEND,
	};

	if (HX_getopt(options_table, argc, argv, HXOPT_USAGEONERR) !=
	    HXOPT_ERR_SUCCESS)
		return false;

	if (intv != 0) {
		pv_req_interval.tv_sec  = intv;
		pv_req_interval.tv_nsec = modf(intv, &intv);
	}
	pv_buffer_size <<= 10;
	pv_disk_window <<= 10;

	if (*argc < 2) {
		fprintf(stderr, "You need to specify some devices to "
		        "keep spinning.\n");
		return false;
	}
	for (arg = &(*argv)[1]; *arg != NULL; ++arg)
		pv_open_device(*arg);
	if (HXlist_empty(&pv_bdev_list)) {
		fprintf(stderr, "ERROR: No devices configured, exiting.\n");
		return false;
	}
	return true;
}

int main(int argc, const char **argv)
{
	int ret;

	if ((ret = HX_init()) < 0) {
		fprintf(stderr, "HX_init: %s\n", strerror(-ret));
		return EXIT_FAILURE;
	}

	if (!pv_get_options(&argc, &argv))
		return EXIT_FAILURE;

	pv_mainloop();
	return EXIT_SUCCESS;
}
