.TH parkverbot 8 "2012-05-14" "Parkverbot project" "Parkverbot User Reference"
.SH Name
parkverbot \(em daemon to inhibit hard disk head parking
.SH Synopsis
\fBparkverbot\fP [\fB\-r\fP \fIkbytes\fP] [\fB\-t\fP \fIsecs\fP]
\fIdevice\fP...
.SH Introduction
.PP
Rotational hard disks have a controversial (mis)feature which automatically
unloads the read/write heads during perceived inactivity, usually within 10
seconds. Western Digital calls it "Intellipark", but the problem is present in
other manufacturers' disks as well, and goes back to at least the mid-2000s.
.PP
Frequent parking leads to noticable latency on wakeup, and the hard retraction
is seen as an impediment to disk life.
Even with various utility programs, this parking feature is not reliably deactivatable.
(See "Comparison" section below.)
.SH Description
.PP
The "parkverbot" daemon will issue small read requests periodically to a random
location on disk in an effort to reset the inactivity timer in the hardware and
so prevent the dreaded head unloading. As such, its operation is
\fBnon-destructive\fP and \fBinteroperable\fP across different brands of hard
drives and transports.
.PP
The current block reading algorithm works reasonably well in practice: the
worst observed head parking rate was about 3 unloads/day (0.125/hour);
best figures hover around 0.5 unloads/day (0.021/hour).
.SH Options
.PP
.TP
\fB\-b\fP \fIsize\fP
The size of a read request, in kilobytes (defaults to 64 KB).
.TP
\fB\-r\fP \fIsize\fP
The guard window size (defaults to 16 MB). When requesting a block off the
disk, the hardware might choose to cache more parts, since it has to hover
anyways and wait over a particular track for the desired sector. If the next
random block we choose happens to be already cached because of that, the head
unload timer will not be reset. To avoid this, if the new random location falls
within \fIsize\fP kilobytes, we shuffle out a new location and retry.
.TP
\fB\-t\fP \fIsecs\fP
The interval in which to request blocks. The default is 4 seconds, and is sort
of the lowest denominator across all the disks personally encountered not to go
into sleep.
.PP
The defaults for -b and -t have little effect on energy use, the prospect here
is just +1% energy use for a 2.5\(aq disk. (For details, feel free to see the
source file of this manpage.)
.\"
.\" Calculation idle-pacing:
.\"
.\" - The disks are rated at 140 MB/s read speed.
.\" - Reading just 64 KB would take 0.0004 seconds.
.\" - At the very minimum, one rotation is always needed.
.\" - At 5400rpm, one rotation already takes 0.0111 seconds.
.\" - Full power is likely used during the entire rotation.
.\" - Full power may be applied during the seek.
.\" - Assuming max. seek latency of 12 ms
.\"
.\" WD5000LPCX (2020, 5400rpm, 2.5", CMR, 0.55W idle, 1.4W active, 12ms seek):
.\" active_time at -t4 = (0.012 + 0.0111) * 15 = 0.34666
.\" 0.55 W over 60 s = 33 J/min
.\" 0.55*(60-at) + 1.4*at = 33.2947 J/min
.\"
.\" ST5000LM000 (2020, 5400rpm, 2.5", SMR, 1.1W idle, 2.1W active, 14ms seek):
.\" at = (0.014 + 0.0111) * 15 = 0.37666
.\" 1.1 W over 60 s = 66 J/min
.\" 1.1*(60-at) + 2.1*at = 66.3767 J/min
.\"
.\" MK2003GAH/HD1364 (2003, 4200rpm, 2.5", CMR, ? idle assuming 0.8W,
.\" 1.4W active, 26ms seek):
.\" at = (0.026 + 0.0143) * 15 = 0.604
.\" 0.8 W over 60 s = 48 J/min
.\" 0.8*(60-at) + 1.4*at = 48.02 J/min
.\"
.SH Configuration
.PP
You can choose between running one parkverbot instance per disk, or one
instance for all disks (reading them is serialized however).
.PP
Two systemd service files are shipped with the package. One is the service
template file "parkverbot@.service", which allows to start one parkverbot
daemon instance per disk by way of:
.nf
  systemctl enable/start/stop/status/disable parkverbot@dev-sda.service
.fi
.PP
The other is the normal service file "parkverbot.service" for a single daemon
for one or more disks, the list of which is to be set in
/etc/sysconfig/parkverbot in the PARKVERBOT_DISKS variable.
.PP
If you do not have systemd, you can still start the daemon in any other
fashion, such as from sysvinit's /etc/init.d/boot.local.
.SH Naming
.PP
The name is the German word for a "no parking" zone.
.SH S.M.A.R.T. info
.PP
Unloading of the heads is measurable by S.M.A.R.T. attribute(s). Some disk
models used number 193, others 225. Both are labeled "Load_Cycle_Count".
.SH Comparison with other utilities
.PP
The "wdidle3" proprietary utility uses a vendor-specific command (VSC) to
change the - infamous - "idle3" timer which controls the parking time. How well
this works with non-WD drives is unknown; if the VSC modifies firmware, it is
probably a very bad idea. The program also requires MS-DOS, which is
increasingly hard to run on contemporary x86-PC systems, and totally
inacceptable on non-x86/non-PC systems. Its effectiveness from within a virtual
machine or emulator using disk/block device passthrough is not known either.
.PP
The `hdparm -B` Linux command may be used to set the APM (Advanced Power
Management) level. However, disks often report to not support this setting, or
if they do, have no effect on unloading, but only acoustic settings. hdparm
also does not work with SCSI disks, SAS disks, disks in enclosures attached
through USB translation layer, or SATA disks on a SAS plane.
(There are dumps for developers in the source distribution.)
.PP
"idle3-tools" is something that I only found recently (2014), though it seems
to exist for slightly longer than parkverbot. Like wdidle3, it uses the VSC.
(http://idle3-tools.sf.net/)
.PP
"wdantiparkd" WD anti-intellipark daemon says it uses a write operation on a
file. That is somewhat unfortunate, as layers of caching in both the operating
system and disk may lead to an inactivity beyond the parking threshold on the
actual head. (http://sagaforce.com/sound/wdantiparkd/)
.PP
parkverbot - this daemon - does normal read requests on the raw block device.
This means there will be no destructive operations, no additional degradation
of the medium, and allows it to be usable with all kinds of rotating hard disks
no matter the manufacturer or interface type. Chunks are read from random new
locations far enough from the old one in an attempt to evade caches. This will
manifest in a short flash of the disk LED (if you have one) every now and then.
By default, one chunk is requested every four seconds, so that two are within 8
seconds (the common WD idle3 time).
