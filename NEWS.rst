v1.4 (2021-10-15)
=================
* Added hardening settings to systemd services


v1.3 (2020-09-04)
=================
Documentation updates only.


v1.2 (2014-12-08)
=================
Documentation updates only.


v1.1 (2013-01-31)
=================
* Make parkverbot compiled in 32-bit run
  (the BLKGETSIZE64 call was not passed a 64-bit object in all cases)
* Do read all disks, rather than one disk, per interval
* Properly store previous read offset per-disk, not global


v1.0 (2012-05-09)
=================
Initial release
